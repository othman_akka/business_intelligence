import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='localhost',database='bd_projet',user='root',password='')
 
    cursor = connection.cursor()
    cursor.execute("select distinct d.annee , d.mois , d.jour , f.price_jour ,l.listing_name from dim_date d,table_fait f,dim_listing l where l.id_listing = f.id_listing and d.id_date = f.id_date ")
    
    records = cursor.fetchall()

    for row in records:
        print(row)
        

except Error as e:
    print("Error reading data from MySQL table", e)
finally:
    if (connection.is_connected()):
        connection.close()
        cursor.close()
        print("MySQL connection is closed")